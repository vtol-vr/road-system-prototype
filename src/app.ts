import { Application, Graphics, Sprite, Texture } from "pixi.js";
import { Viewport } from 'pixi-viewport';
import { GroupedRoadFeatures, Position, ProcessSteps, RoadFeature } from "./types";
import { coordinateToX, coordinateToY, distance, xInMap, yInMap } from "./utilities";
import deepcopy from 'deepcopy';
import * as randomColor from 'randomcolor';
import * as features from './features.geo.json';
import * as heightmap from './heightmap.json';
import RoadBuilder from './builder';

export default class App {
  app: Application = new Application({ width: 640, height: 480 });
  viewport: Viewport;
  step: ProcessSteps = ProcessSteps.unrendered;
  nextStep: ProcessSteps = ProcessSteps.raw;
  worldWidth: number = (heightmap.settings.chunks * 20) + 1;
  graphics: Graphics = new Graphics();
  roadFeatures: RoadFeature[] = [];
  groupedRoadFeatures: GroupedRoadFeatures[] = [];
  roadBuilder: RoadBuilder;
  constructor() {
    document.body.appendChild(this.app.view as any);
    this.app.resizeTo = window;

    this.viewport = new Viewport({
      screenWidth: window.innerWidth,
      screenHeight: window.innerHeight,
      worldWidth: this.worldWidth,
      worldHeight: this.worldWidth,
      events: this.app.renderer.events
    });
    this.viewport.fit();
    this.viewport.moveCenter(this.worldWidth / 2, this.worldWidth / 2);
    this.app.stage.addChild(this.viewport);
    this.viewport.drag();
    this.viewport.pinch();
    this.viewport.wheel();
    this.viewport.decelerate();

    const background = this.viewport.addChild(new Sprite(Texture.WHITE));
    background.width = this.worldWidth;
    background.height = this.worldWidth;

    this.viewport.addChild(this.graphics);
    this.process();
    this.app.ticker.add(() => {
      if(this.step !== this.nextStep) {
        this.render();
      }
      this.step = this.nextStep;
    });
    window.addEventListener('keypress', (e) => {
      if(e.key === 'd') {
        this.nextStep++;
      }
      if(e.key === 'a') {
        this.nextStep--;
      }
    });
  }
  process() {
    //Filter out everything but major roads
    for(let feature of features as any) {
      if(feature.properties.kind === 'major_road' && (feature.geometry.type === 'LineString' || feature.geometry.type === 'MultiLineString')) {
        if(feature.geometry.type === 'LineString') {
          feature.geometry.coordinates = [feature.geometry.coordinates];
        }
        for(let lineString of feature.geometry.coordinates) {
          let coordinates: Position[] = lineString.map(c => [
            coordinateToX(c[0], heightmap.settings.chunks, heightmap.settings.bounds),
            coordinateToY(c[1], heightmap.settings.chunks, heightmap.settings.bounds)
          ]).filter(c => (
            xInMap(c[0], heightmap.settings.chunks, heightmap.settings.edgeMode, heightmap.settings.coastSide) &&
            yInMap(c[1], heightmap.settings.chunks, heightmap.settings.edgeMode, heightmap.settings.coastSide)
          )).map(c => {
            return { x: c[0], y: c[1], z: 0 };
          });
          let length = 0;
          for(let i = 1;i < coordinates.length;i++) {
            length += distance(coordinates[i - 1], coordinates[i]);
          }
          this.roadFeatures.push({
            ref: feature.properties.ref,
            lanes: 4,
            properties: deepcopy(feature.properties),
            length: length,
            coordinates: coordinates
          });
        }
      }
    }
    //Group together road features and sort by length
    for(let roadFeature of this.roadFeatures) {
      if(typeof roadFeature.ref === 'string') {
        let foundGroup = false;
        for(let groupedRoadFeature of this.groupedRoadFeatures) {
          if(groupedRoadFeature.ref === roadFeature.ref) {
            groupedRoadFeature.features.push(roadFeature);
            foundGroup = true;
          }
        }
        if(!foundGroup) {
          this.groupedRoadFeatures.push({
            ref: roadFeature.ref,
            features: [roadFeature]
          });
        }
      }
    }
    for(let groupedRoadFeature of this.groupedRoadFeatures) {
      groupedRoadFeature.features.sort((a, b) => b.length - a.length);
    }
    //Create road graph
    this.roadBuilder = new RoadBuilder(this.groupedRoadFeatures);
  }
  render() {
    this.graphics.clear();
    //No processing, just render raw line strings from data
    if(this.nextStep === ProcessSteps.raw) {
      for(let roadFeature of this.roadFeatures) {
        let color = randomColor();
        this.renderLineString(roadFeature.coordinates, color, 0.2, 0x000000, 0.1);
      }
    }
    //Render grouped roads
    if(this.nextStep === ProcessSteps.grouped) {
      for(let groupedRoadFeature of this.groupedRoadFeatures) {
        let color = randomColor();
        for(let roadFeature of groupedRoadFeature.features) {
          this.renderLineString(roadFeature.coordinates, color, 0.2, 0x000000, 0.1);
        }
      }
    }
    //Render merged road graph
    if(this.nextStep === ProcessSteps.merge) {
      this.roadBuilder.render(this.graphics, 0.2, 0x000000, 0xff0000, 0.1);
    }
  }
  renderLineString(coordinates: Position[], lineColor: number, width: number, dotColor: number, radius: number) {
    this.graphics.lineStyle({
      width: width,
      color: lineColor
    });
    if(coordinates.length > 0) {
      this.graphics.moveTo(coordinates[0].x, coordinates[0].y);
      for(let i = 1;i < coordinates.length;i++) {
        this.graphics.lineTo(coordinates[i].x, coordinates[i].y);
      }
      this.graphics.lineStyle({
        width: 0,
        color: dotColor
      });
      this.graphics.beginFill(dotColor);
      for(let coordinate of coordinates) {
        this.graphics.drawCircle(coordinate.x, coordinate.y, radius);
      }
      this.graphics.endFill();
    }
  }
}