export enum ProcessSteps {
  unrendered = -1,
  raw = 0,
  grouped = 1,
  merge = 2,
  intersects = 3,
  bridge = 4,
  bezier = 5,
  segment = 6
};

export interface RoadFeature {
  ref: string;
  lanes: number;
  properties: any;
  length: number;
  coordinates: Position[];
};

export interface GroupedRoadFeatures {
  ref: string;
  features: RoadFeature[];
};

export interface RoadGraph {
  roads: Road[];
};

export interface Road {
  ref: string;
  nodes: RoadNode[];
};

export interface RoadNode {
  id: number;
  bridge: boolean;
  intersection: boolean;
  lanes: number;
  neighbors: RoadNode[];
  position: Position;
};

export interface Position {
  x: number;
  y: number;
  z: number;
};