import { Position } from './types';

export function coordinateToX(lng: number, chunks: number, bounds): number {
  let mapWidth = (chunks * 20) + 1;
  let lngWidth = Math.abs(bounds.lngEast - bounds.lngWest);
  return ((lng - bounds.lngWest) * (mapWidth - 1)) / lngWidth;
}

export function coordinateToY(lat: number, chunks: number, bounds): number {
  let mapWidth = (chunks * 20) + 1;
  let latHeight = Math.abs(bounds.latNorth - bounds.latSouth);
  return (-(lat - bounds.latNorth) * (mapWidth - 1)) / latHeight;
}

export function xInMap(x: number, chunks: number, edgeMode: string, coastSide: string): boolean {
  let mapWidth = chunks * 20 + 1;
  let leftBoundary = mapWidth * 0.15;
  if(edgeMode === 'Water' || (edgeMode === 'Coast' && coastSide === 'West')) {
    leftBoundary = mapWidth * 0.1;
  }
  let rightBoundary = mapWidth * 0.15;
  if(edgeMode === 'Water' || (edgeMode === 'Coast' && coastSide === 'East')) {
    rightBoundary = mapWidth * 0.1;
  }
  return x >= leftBoundary && x <= (mapWidth - rightBoundary);
}

export function yInMap(y: number, chunks: number, edgeMode: string, coastSide: string): boolean {
  let mapWidth = chunks * 20 + 1;
  let topBoundary = mapWidth * 0.15;
  if(edgeMode === 'Water' || (edgeMode === 'Coast' && coastSide === 'North')) {
    topBoundary = mapWidth * 0.1;
  }
  let bottomBoundary = mapWidth * 0.15;
  if(edgeMode === 'Water' || (edgeMode === 'Coast' && coastSide === 'South')) {
    bottomBoundary = mapWidth * 0.1;
  }
  return y >= topBoundary && y <= (mapWidth - bottomBoundary);
}

export function distance(p1: Position, p2: Position): number {
  return Math.sqrt(
    ((p1.x - p2.x) * (p1.x - p2.x)) +
    ((p1.y - p2.y) * (p1.y - p2.y)) +
    ((p1.z - p2.z) * (p1.z - p2.z))
  );
}

export function dotProduct(v1: Position, v2: Position): number {
  return (
    (v1.x * v2.x) +
    (v1.y * v2.y) +
    (v1.z * v2.z)
  );
}

export function closestPoint(lineStart: Position, lineEnd: Position, targetPoint: Position): Position {
  //Do vector projection to figure out closest point
  let vectorA: Position = {
    x: targetPoint.x - lineStart.x,
    y: targetPoint.y - lineStart.y,
    z: targetPoint.z - lineStart.z
  };
  let vectorB: Position = {
    x: lineEnd.x - lineStart.x,
    y: lineEnd.y - lineStart.y,
    z: lineEnd.z - lineStart.z
  };
  let scale = dotProduct(vectorA, vectorB) / dotProduct(vectorB, vectorB);
  let projA: Position = {
    x: vectorB.x * scale,
    y: vectorB.y * scale,
    z: vectorB.z * scale
  };
  let vectorLength = distance({ x: 0, y: 0, z: 0 }, vectorB);
  let projLength = distance({ x: 0, y: 0, z: 0 }, projA);
  if(projLength > vectorLength) {
    return lineEnd;
  }
  if(projLength < 0) {
    return lineStart;
  }
  return {
    x: projA.x + lineStart.x,
    y: projA.y + lineStart.y,
    z: projA.z + lineStart.z
  };
}