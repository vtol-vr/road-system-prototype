import { Graphics } from "pixi.js";
import { GroupedRoadFeatures, Position, Road, RoadGraph, RoadNode } from "./types";
import { closestPoint, distance } from './utilities';
import * as randomColor from 'randomcolor';

export default class RoadBuilder {
  groupedRoads: GroupedRoadFeatures[];
  graph: RoadGraph = {
    roads: []
  };
  isSameDistanceThreshold: number = 0.1;
  maxConnectDistance: number = 25;
  constructor(groupedRoads: GroupedRoadFeatures[]) {
    this.groupedRoads = groupedRoads;
    this.process();
  }
  process() {
    let gid = 0;
    for(let groupedRoad of this.groupedRoads) {
      let roadPieces: Road[] = [];
      for(let roadFeature of groupedRoad.features) {
        let roadPiece: Road = {
          ref: roadFeature.ref,
          nodes: []
        };
        for(let i = 0;i < roadFeature.coordinates.length;i++) {
          roadPiece.nodes.push({
            id: gid,
            bridge: false,
            intersection: false,
            lanes: roadFeature.lanes,
            neighbors: [],
            position: roadFeature.coordinates[i]
          });
          gid++;
          if(i > 0) {
            roadPiece.nodes[i].neighbors.push(roadPiece.nodes[i - 1]);
            roadPiece.nodes[i - 1].neighbors.push(roadPiece.nodes[i]);
          }
        }
        roadPieces.push(roadPiece);
      }
      //Filter out road pieces with one or less nodes
      roadPieces = roadPieces.filter(r => r.nodes.length > 1);
      //Connect roadPieces together
      if(roadPieces.length > 0) {
        let connectedRoads = this.connectRoads(roadPieces);
        for(let i = 0;i < connectedRoads.length;i++) {
          connectedRoads[i].ref += '_' + i;
          this.graph.roads.push(connectedRoads[i]);
        }
      }
    }
  }
  render(graphics: Graphics, width: number, dotColor: number, intersectionColor: number, radius: number) {
    for(let road of this.graph.roads) {
      let color = randomColor();
      let edges = this.roadEdges(road);
      graphics.lineStyle({
        width: width,
        color: color
      });
      for(let edge of edges) {
        graphics.moveTo(edge[0].position.x, edge[0].position.y);
        graphics.lineTo(edge[1].position.x, edge[1].position.y);
      }
      graphics.lineStyle({
        width: 0,
        color: dotColor
      });
      for(let node of road.nodes) {
        graphics.beginFill(dotColor);
        if(node.neighbors.length > 2) {
          graphics.beginFill(intersectionColor);
        }
        graphics.drawCircle(node.position.x, node.position.y, radius);
      }
      graphics.endFill();
    }
  }
  connectRoads(roadPieces: Road[]): Road[] {
    //Fix nodes pass
    for(let roadPiece of roadPieces) {
      roadPiece = this.correctRoadNodes(roadPiece);
    }
    let didConnect = true;
    while(didConnect) {
      didConnect = false;
      //Find unique nodes
      let uniqueRoadPieces: Road[] = [];
      for(let i = 0;i < roadPieces.length;i++) {
        uniqueRoadPieces.push(roadPieces[i]);
        for(let j = i + 1;j < roadPieces.length;j++) {
          if(i != j && this.roadsConnected(roadPieces[i], roadPieces[j])) {
            roadPieces.splice(j, 1);
            j--;
          }
        }
      }
      roadPieces = uniqueRoadPieces;
      //Iterate through road pieces to create connections as needed
      for(let basePiece of roadPieces) {
        let otherPieces: Road[] = [];
        for(let roadPiece of roadPieces) {
          if(roadPiece != basePiece) {
            otherPieces.push(roadPiece);
          }
        }
        //Find closest node pairs and make an edge
        let minRoadPiece: Road = { ref: '', nodes: [] };
        let minNodePair: RoadNode[] = [];
        let minDistance = Number.POSITIVE_INFINITY;
        for(let otherPiece of otherPieces) {
          let currNodePair = this.closestNodesBetweenRoads(basePiece, otherPiece);
          if(currNodePair.length === 2) {
            let currDistance = distance(currNodePair[0].position, currNodePair[1].position);
            if(currDistance < minDistance) {
              minRoadPiece = otherPiece;
              minNodePair = currNodePair;
              minDistance = currDistance;
            }
          }
        }
        //Connect roads if min distance is not bigger than the max connection distance allowed
        if(minNodePair.length === 2 && minDistance <= this.maxConnectDistance) {
          didConnect = true;
          minNodePair[0].neighbors.push(minNodePair[1]);
          minNodePair[1].neighbors.push(minNodePair[0]);
          basePiece = this.correctRoadNodes(basePiece);
          for(let i = 0;i < roadPieces.length;i++) {
            if(roadPieces[i] == minRoadPiece) {
              roadPieces.splice(i, 1);
              i--;
            }
          }
          break;
        }
      }
    }
    //Last fix nodes pass
    for(let roadPiece of roadPieces) {
      roadPiece = this.correctRoadNodes(roadPiece);
    }
    return roadPieces;
  }
  //Closest nodes between two roads
  closestNodesBetweenRoads(road1: Road, road2: Road): RoadNode[] {
    if(road1.nodes.length <= 0 || road2.nodes.length <= 0) {
      return [];
    }
    let minRoad1Node = road1.nodes[0];
    let minRoad2Node = road2.nodes[0];
    let minDistance = Number.POSITIVE_INFINITY;
    for(let road1Node of road1.nodes) {
      for(let road2Node of road2.nodes) {
        let currDistance = distance(road1Node.position, road2Node.position);
        if(currDistance < minDistance) {
          minRoad1Node = road1Node;
          minRoad2Node = road2Node;
          minDistance = currDistance;
        }
      }
    }
    return [minRoad1Node, minRoad2Node];
  }
  //Merge same position nodes and fix road.nodes by traversing and looking at all connected nodes
  correctRoadNodes(road: Road): Road {
    if(road.nodes.length <= 1) {
      return road;
    }
    //Pick a node to be first
    let done = false;
    let currNode: RoadNode = road.nodes[0];
    let nextNodes: RoadNode[] = road.nodes.slice(1);
    let visitedNodes: RoadNode[] = [];
    while(!done) {
      //Mark current node as visited
      visitedNodes.push(currNode);
      //Add non-visited (and non-next) neighbors as next nodes to look at
      for(let neighbor of currNode.neighbors) {
        let found = false;
        for(let nextNode of nextNodes) {
          if(nextNode.id === neighbor.id) {
            found = true;
          }
        }
        for(let visitedNode of visitedNodes) {
          if(visitedNode.id === neighbor.id) {
            found = true;
          }
        }
        if(!found) {
          nextNodes.push(neighbor);
        }
      }
      //Move to next node
      if(nextNodes.length > 0) {
        currNode = nextNodes[0];
        nextNodes.splice(0, 1);
      }
      else {
        done = true;
      }
    }
    road.nodes = visitedNodes;
    //Merge any nodes with same position
    for(let i = 0;i < road.nodes.length;i++) {
      for(let j = i + 1;j < road.nodes.length;j++) {
        if(i != j && distance(road.nodes[i].position, road.nodes[j].position) <= this.isSameDistanceThreshold) {
          //Merge neighbors from duplicate nodes into single node
          let newNeighbors = road.nodes[i].neighbors;
          for(let neighbor of road.nodes[j].neighbors) {
            if(newNeighbors.filter(n => n.id === neighbor.id).length <= 0) {
              newNeighbors.push(neighbor);
            }
          }
          road.nodes[i].neighbors = newNeighbors;
          road.nodes.splice(j, 1);
          j--;
          //Verify neighbors have single node listed as a neighbor
          for(let neighbor of road.nodes[i].neighbors) {
            if(neighbor.neighbors.filter(n => n.id === road.nodes[i].id).length <= 0) {
              neighbor.neighbors.push(road.nodes[i]);
            }
          }
        }
      }
    }
    //Remove duplicate neighbors
    for(let currNode of road.nodes) {
      let uniqueNeighbors: RoadNode[] = [];
      for(let neighbor of currNode.neighbors) {
        if(uniqueNeighbors.filter(n => n.id === neighbor.id).length <= 0 && neighbor.id !== currNode.id) {
          uniqueNeighbors.push(neighbor);
        }
      }
      currNode.neighbors = uniqueNeighbors;
    }
    return road;
  }
  //Return all edges in a road
  roadEdges(road: Road): RoadNode[][] {
    let edges: RoadNode[][] = [];
    if(road.nodes.length <= 1) {
      return edges;
    }
    //Pick a node to be first
    let done = false;
    let currNode: RoadNode = road.nodes[0];
    let nextNodes: RoadNode[] = road.nodes.slice(1);
    let visitedNodes: RoadNode[] = [];
    while(!done) {
      //Create edges
      for(let neighbor of currNode.neighbors) {
        if(visitedNodes.filter(n => n.id === neighbor.id).length <= 0) {
          edges.push([currNode, neighbor]);
        }
      }
      //Mark current node as visited
      visitedNodes.push(currNode);
      //Add non-visited (and non-next) neighbors as next nodes to look at
      for(let neighbor of currNode.neighbors) {
        let found = false;
        for(let nextNode of nextNodes) {
          if(nextNode.id === neighbor.id) {
            found = true;
          }
        }
        for(let visitedNode of visitedNodes) {
          if(visitedNode.id === neighbor.id) {
            found = true;
          }
        }
        if(!found) {
          nextNodes.push(neighbor);
        }
      }
      //Move to next node
      if(nextNodes.length > 0) {
        currNode = nextNodes[0];
        nextNodes.splice(0, 1);
      }
      else {
        done = true;
      }
    }
    //Remove duplicate edges
    let uniqueEdges: RoadNode[][] = [];
    let uniqueEdgeId: string[] = [];
    for(let edge of edges) {
      let edgeId = edge.sort((a, b) => a.id - b.id).map(n => n.id.toString()).reduce((p, c) => p + '_' + c);
      if(!uniqueEdgeId.includes(edgeId)) {
        uniqueEdges.push(edge);
        uniqueEdgeId.push(edgeId)
      }
    }
    return uniqueEdges;
  }
  //Check if two roads are connected
  roadsConnected(road1: Road, road2: Road): boolean {
    road1 = this.correctRoadNodes(road1);
    road2 = this.correctRoadNodes(road2);
    for(let node1 of road1.nodes) {
      for(let node2 of road2.nodes) {
        if(node1.id === node2.id) {
          return true;
        }
      }
    }
    return false;
  }
  //Returns array of grouped edges that contain only two ends or intersections
  roadEdgeCollection(road: Road): RoadNode[][][] {
    let edges = this.roadEdges(road);
    let added: RoadNode[][] = [];
    let groupedEdges: RoadNode[][][] = [];
    let finished: boolean[] = [];
    //Add only edges with at least one node being an intersection or end
    for(let edge of edges) {
      let intersectOrEndEdges = edge.filter(n => n.neighbors.length === 1 || n.neighbors.length > 2);
      if(intersectOrEndEdges.length > 0) {
        groupedEdges.push([edge]);
        added.push(edge);
        //Mark as finished if both nodes in the edge is an intersection or end
        finished.push(intersectOrEndEdges.length >= 2);
      }
    }
    while(added.length < edges.length) {
      //Sort by length ascending
      for(let i = 0;i < groupedEdges.length;i++) {
        for(let j = i + 1;j < groupedEdges.length;j++) {
          if(groupedEdges[i].length > groupedEdges[j].length) {
            let tempGE = groupedEdges[j];
            let tempF = finished[j];
            groupedEdges[j] = groupedEdges[i];
            finished[j] = finished[i];
            groupedEdges[i] = tempGE;
            finished[i] = tempF;
          }
        }
      }
      //Add edges to grouped edges
      let didAdd = false;
      for(let edge of edges) {
        for(let i = 0;i < groupedEdges.length;i++) {
          let groupedEdge = groupedEdges[i];
          if(!didAdd && !finished[i] && !added.includes(edge)) {
            //Check if grouped edge shares a node with current edge
            let sharesNode = false;
            for(let edge2 of groupedEdge) {
              for(let node of edge) {
                for(let node2 of edge2) {
                  if(node.id === node2.id) {
                    sharesNode = true;
                  }
                }
              }
            }
            if(sharesNode) {
              let intersectOrEndEdges = edge.filter(n => n.neighbors.length === 1 || n.neighbors.length > 2);
              groupedEdge.push(edge);
              added.push(edge);
              finished[i] = intersectOrEndEdges.length > 0;
              didAdd = true;
            }
          }
        }
      }
      //Merge any groups that connect
      for(let i = 0;i < groupedEdges.length;i++) {
        let groupedEdge = groupedEdges[i];
        if(finished[i]) { continue; }
        //Get all non intersection or end nodes in the grouped edge
        let nonIntersectOrEndNodes: RoadNode[] = [];
        for(let edge of groupedEdge) {
          for(let node of edge) {
            if(node.neighbors.length === 2 && nonIntersectOrEndNodes.filter(n => n.id === node.id).length <= 0) {
              nonIntersectOrEndNodes.push(node);
            }
          }
        }
        //Find all other unfinished grouped edges that share non intersection nodes
        let groupedEdgesToMergeIndices: number[] = [];
        for(let j = i + 1;j < groupedEdges.length;j++) {
          let otherGroupedEdge = groupedEdges[j];
          if(i != j && !finished[j]) {
            for(let edge of otherGroupedEdge) {
              for(let node of edge) {
                if(nonIntersectOrEndNodes.filter(n => n.id === node.id).length > 0 && !groupedEdgesToMergeIndices.includes(j)) {
                  groupedEdgesToMergeIndices.push(j);
                }
              }
            }
          }
        }
        //Merge other grouped edges
        for(let groupedEdgesToMergeIndex of groupedEdgesToMergeIndices) {
          let otherGroupedEdge = groupedEdges[groupedEdgesToMergeIndex];
          for(let edge of otherGroupedEdge) {
            if(!groupedEdge.includes(edge)) {
              groupedEdge.push(edge);
              finished[i] = true;
            }
          }
        }
        //Remove other grouped edges used in the merge
        groupedEdgesToMergeIndices.sort((a, b) => b - a);
        for(let groupedEdgesToMergeIndex of groupedEdgesToMergeIndices) {
          groupedEdges.splice(groupedEdgesToMergeIndex, 1);
          finished.splice(groupedEdgesToMergeIndex, 1);
          if(groupedEdgesToMergeIndex < i) {
            i--;
          }
        }
      }
    }
    //Sort grouped edges by distance (shortest first)
    let groupEdgeInfo: { index: number; distance: number; }[] = [];
    //for(let groupedEdge of )
    return groupedEdges;
  }
}